// 1. Екранування - спосіб запису, щоб компілятор міг правильно розпізнати рядкові літерали в коді
// Ми додаємо перед лапками всередині рядка тексту символ \ (бекслеш)
// Наприклад - alert("The best programming language - \"javascript\"")

// 2. function declaration statement (function name())
// function definition expression (let name = function())
// arrow functions

// 3. Нoisting (підняття) - це механізм JavaScript, в якому змінні і оголошення функцій, пересуваються вгору своєї області видимості перед тим, як код буде виконано
// Це означає те, що зовсім неважливо де були оголошені функція або змінні, всі вони пересуваються вгору своєї області видимості, незалежно від того, локальна вона або глобальна

function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name: "),
    lastName: prompt("Enter your last name: "),
    birthday: prompt("Enter your birtday", "dd.mm.yyyy"),
    get getLogin() {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
    get getAge() {
      return new Date().getFullYear() - this.birthday.slice(6);
    },
    get getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6)
      );
    },
  };
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin);
console.log(user.getAge);
console.log(user.getPassword);
